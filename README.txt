CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------
This module adds integration Twitter-Post-Fetcher in Drupal.
Twitter-Post-Fetcher get your tweets and displaying on your website using JavaScript,
without using Twitter 1.1 API.


REQUIREMENTS
------------
Libraries 2.0 or later
Jquery Update : You must run JQuery version 1.10 or later.

INSTALLATION
------------
* Download and enable the module.
 (See:https://drupal.org/documentation/install/modules-themes/modules-7
 for further information.)
* Download the Twitter-Post-Fetcher from
 https://github.com/jasonmayes/Twitter-Post-Fetcher
 Direct download link: https://github.com/jasonmayes/Twitter-Post-Fetcher/archive/master.zip
* Extract the downloaded zip to a folder in your libraries folder
(usually sites/all/libraries).
* rename the Twitter-Post-Fetcher-master folder to 'Twitter-Post-Fetcher'.
* Check the path to the Twitter-Post-Fetcher javascript. It should be
DOCROOT/sites/all/libraries/Twitter-Post-Fetcher/js/twitterFetcher_min.js
[adjust this path if your libraries folder is in a different location]

CONFIGURATION
-------------
For module configuration please go to: <yourdomain>/admin/config/services/simple_tweets

MAINTAINERS
-----------
Current maintainers:
 * Denis Pushkarev https://www.drupal.org/user/3370000
