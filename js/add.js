/**
 * @file
 * @ This file include recived data from drupal and call twitterFetcher function.
 */
(function ($) {
  Drupal.behaviors.twitterFetcher = {
        attach: function (context, settings) {
          var config = {
                "id": Drupal.settings.simple_tw_id,
                "domId": 'block-simple-tweets-t-w-block',
                "maxTweets": Drupal.settings.simple_tw_max,
                "enableLinks": Drupal.settings.simple_tw_hyperlink,
                "showPermalinks": false,
                "showUser": Drupal.settings.simple_tw_user,
                "showTime": Drupal.settings.simple_tw_time,
                "showRetweet": Drupal.settings.simple_tw_retweet,
                "showInteraction": Drupal.settings.simple_tw_interact,
                "showImages": Drupal.settings.simple_tw_img,
                "lang": Drupal.settings.simple_tw_lang,
                "linksInNewWindow": Drupal.settings.simple_tw_wind,
                "Permalinks": Drupal.settings.simple_tw_prema
            };
          twitterFetcher.fetch(config);

        }
  };
})(jQuery);
