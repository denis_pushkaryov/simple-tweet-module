<?php

/**
 * @file
 * This admin menus file from Simple tweets module.
 */

/**
 * Implements hook_form().
 */
function simple_tweets_form($form, &$form_state) {

  $form['simple_tw_id'] = array(
    '#title'         => t('ID'),
    '#description'   => t('Insert ID of the Twitter widget'),
    '#type'          => 'textfield',
    '#default_value' => variable_get('simple_tw_id'),
    '#required'      => TRUE,
    '#size' => 20,
    '#maxlength' => 20,
  );

  $form['simple_tw_max'] = array(
    '#title'         => t('Max tweets'),
    '#description'   => t('Number between 1 and 20'),
    '#type'          => 'textfield',
    '#default_value' => variable_get('simple_tw_max'),
    '#required'      => TRUE,
    '#size' => 10,
    '#maxlength' => 2,
  );

  $form['simple_tw_hyperlink'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable hyperlinks'),
    '#description' => t('Urls and hashtags to be hyperlinked'),
    '#default_value' => variable_get('simple_tw_hyperlink'),
  );

  $form['simple_tw_user'] = array(
    '#type' => 'checkbox',
    '#title' => t('User'),
    '#description' => t('Show user photo / name for tweet'),
    '#default_value' => variable_get('simple_tw_user'),
  );

  $form['simple_tw_time'] = array(
    '#type' => 'checkbox',
    '#title' => t('Time'),
    '#description' => t('Show time of tweet'),
    '#default_value' => variable_get('simple_tw_time'),
  );

  $form['simple_tw_retweet'] = array(
    '#type' => 'checkbox',
    '#title' => t('Retweets'),
    '#description' => t('Show retweets'),
    '#default_value' => variable_get('simple_tw_retweet'),
  );

  $form['simple_tw_interact'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable interaction'),
    '#description' => t('Show links for reply, retweet and favourite'),
    '#default_value' => variable_get('simple_tw_interact'),
  );

  $form['simple_tw_img'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable images'),
    '#description' => t('Show images from tweet'),
    '#default_value' => variable_get('simple_tw_img'),
  );

  $form['simple_tw_wind'] = array(
    '#type' => 'checkbox',
    '#title' => t('Links in new window'),
    '#description' => t('Open links in new widows'),
    '#default_value' => variable_get('simple_tw_wind'),
  );

  $form['simple_tw_prema'] = array(
    '#type' => 'checkbox',
    '#title' => t('Premalinks'),
    '#description' => t('Enable premalinks'),
    '#default_value' => variable_get('simple_tw_prema'),
  );
  /**
   * The list of languages from twitter api v 1.1.0.
   */
  $form['simple_tw_lang'] = array(
    '#type' => 'select',
    '#title' => t('Language'),
    '#options' => array(

      'Fr' => ('French'),

      'en' => ('English'),

      'ar' => ('Arabic'),

      'ja' => ('Japanese'),

      'es' => ('Spanish'),

      'de' => ('German'),

      'it' => ('Italian'),

      'id' => ('Indonesian'),

      'pt' => ('Portuguese'),

      'ko' => ('Korean'),

      'tr' => ('Turkish'),

      'ru' => ('Russian'),

      'nl' => ('Dutch'),

      'fil' => ('Filipino'),

      'msa' => ('Malay'),

      'zh-tw' => ('Traditional Chinese'),

      'zh-cn' => ('Simplified Chinese'),

      'hi' => ('Hindi'),

      'no' => ('Norwegian'),

      'sv' => ('Swedish'),

      'fi' => ('Finnish'),

      'da' => ('Danish'),

      'pl' => ('Polish'),

      'hu' => ('Hungarian'),

      'fa' => ('Persian'),

      'he' => ('Hebrew'),

      'th' => ('Thai'),

      'uk' => ('Ukrainian'),

      'cs' => ('Czech'),

      'ro' => ('Romanian'),

      'en-gb' => ('British English'),

      'vi' => ('Vietnamese'),

      'bn' => ('Bengali'),

    ),
    '#default_value' => variable_get('simple_tw_lang'),
  );

  return system_settings_form($form);
}

/**
 * This validate function.
 */
function simple_tweets_form_validate($form, &$form_state) {

  $id_key = $form_state['values']['simple_tw_id'];
  $id_pattern = '/[0-9]+$/';

  if (!@preg_match($id_pattern, $id_key)) {
    form_set_error('simple_tw_id', t('Wiget number - only figures'));
  }

  $maxpost_key = $form_state['values']['simple_tw_max'];

  if (!is_numeric($maxpost_key) || $maxpost_key > 20 || $maxpost_key < 1) {
    form_set_error('simple_tw_max', t('Only integers from 1 to 20'));
  }

}
