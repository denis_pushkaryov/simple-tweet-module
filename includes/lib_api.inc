<?php

/**
 * @file
 * This file include libraries API functions from Simple tweets module.
 */

/**
 * Implements hook_libraries_info().
 */
function simple_tweets_libraries_info() {
  $libraries['Twitter-Post-Fetcher'] = array(
    'name' => 'Twitter-Post-Fetcher',
    'vendor url' => 'https://github.com/jasonmayes/Twitter-Post-Fetcher',
    'download url' => 'https://github.com/jasonmayes/Twitter-Post-Fetcher/archive/master.zip',
    'version arguments' => array(
      'file' => 'js/twitterFetcher.js',
      'pattern' => '/v(\d+\.+\d+)/',
      'lines' => 3,
    ),
    'files' => array(
      'js' => array(
        'js/twitterFetcher_min.js',
      ),
      'css' => array(
        'css/style.css',
      ),
    ),
  );

  return $libraries;
}

$library = libraries_detect('Twitter-Post-Fetcher');
if (!empty($library)) {
}

else {
  drupal_set_message(t('Twitter-Post-Fetcher library is not installed'), 'error');
}
